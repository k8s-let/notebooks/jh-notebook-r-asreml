ARG CI_PROJECT_DIR

FROM registry.ethz.ch/k8s-let/notebooks/jh-notebook-r-asreml:3.0.0-03

USER root

RUN PIP_PROXY=http://proxy.ethz.ch:3128 pip3 install -U --proxy=http://proxy.ethz.ch:3128 --default-timeout=100 \
  geojson \
  jupyterlab-deck \
  jupyterlab-git>=0.50.0 \
  pillow \
  rawpy \
  yolov5 \
  rise

USER 1000
